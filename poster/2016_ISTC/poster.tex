%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Portrait Poster
% LaTeX Template
% Version 1.0 (15/5/13)
%
% Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0paper,portrait,fontscale=0.32]{baposter}
\usepackage[scaled]{helvet}
\usepackage{url}
\renewcommand\familydefault{\sfdefault} 

\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{relsize} % Used for making text smaller in some places
\usepackage{enumitem} % list item spacing
\usepackage{lipsum}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{array}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{tkz-kiviat}
\usepackage{natbib}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{array}
\usepackage{enumitem}
\usetikzlibrary{arrows}

%\graphicspath{{pics/}} % Directory in which figures are stored

\setlist{leftmargin=*,noitemsep}

\definecolor{bordercol}{RGB}{40,40,40} % Border color of content boxes
\definecolor{headercol1}{RGB}{186,215,230} % Background color for the header in the content boxes (left side)
\definecolor{headercol2}{RGB}{80,80,80} % Background color for the header in the content boxes (right side)
\definecolor{headerfontcol}{RGB}{0,0,0} % Text color for the header text in the content boxes
%\definecolor{boxcolor}{RGB}{186,215,230} % Background color for the content in the content boxes
\definecolor{boxcolor}{RGB}{255,255,255} % Background color for the content in the content boxes

\definecolor{RY1}{RGB}{226,0,38}
\definecolor{RY2}{RGB}{254,205,27}
\definecolor{Bl1}{RGB}{38,109,131}
\definecolor{Bl2}{RGB}{136,204,207}
\definecolor{OY1}{RGB}{236,117,40}
\definecolor{OY2}{RGB}{254,207,83}
\definecolor{DB1}{RGB}{28,70,114}
\definecolor{DB2}{RGB}{0,147,196}
\definecolor{Vi1}{RGB}{114,43,74}
\definecolor{Vi2}{RGB}{198,132,116}
\definecolor{PG1}{RGB}{133,146,66}
\definecolor{PG2}{RGB}{199,213,79}
\definecolor{Gr1}{RGB}{91,94,111}
\definecolor{Gr2}{RGB}{162,181,198}
\definecolor{Gn1}{RGB}{44,152,46}
\definecolor{Gn2}{RGB}{157,193,7}

\newcommand{\R}{\textsuperscript{\textregistered}}
\newcommand{\TM}{\textsuperscript{\texttrademark}}

\begin{document}

\background{ % Set the background to an image (background.pdf)
%\begin{tikzpicture}[remember picture,overlay]
%\draw (current page.north west)+(-2em,2em) node[anchor=north west]
%{\includegraphics[height=1.1\textheight]{figures/background}};
%\end{tikzpicture}
}

\begin{poster}{
grid=false,
%borderColor=bordercol, % Border color of content boxes
%headerColorOne=headercol1, % Background color for the header in the content boxes (left side)
%headerColorTwo=headercol2, % Background color for the header in the content boxes (right side)
%headerFontColor=headerfontcol, % Text color for the header text in the content boxes
%boxColorOne=boxcolor, % Background color for the content in the content boxes
bgColorOne=DB2!60,
bgColorTwo=Gn2!60,
borderColor=Vi1!85,
headerColorOne=Bl2!50,
headerColorTwo=Bl1!50,
headerFontColor=DB1,
boxColorOne=Vi2!05,
headershape=roundedright, % Specify the rounded corner in the content box headers
headerfont=\large\sf\bf, % Font modifiers for the text in the content box headers
textborder=none,
background=shadetb,
headerborder=none,
boxshade=plain
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
{\sf\bf\huge Beyond Gbps Turbo Decoder on Multi-Core CPUs} % Poster title
{\vspace{1em} Adrien Cassagne, Thibaud Tonnellier, Camille Leroux,\\ Bertrand Le Gal, Olivier Aumage and Denis Barthou% Author names
} % Author email addresses
{\includegraphics[trim=2cm 1cm 2cm 1cm,clip=true,scale=0.15]{pics/logo}} % University/lab logo

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
\headerbox{Software Implementations of FEC decoders}{name=introduction,column=0,row=0,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{
  \textbf{Growing interest for Software Defined Radio (SDR)}
  
  \smallskip

  %------------------------------------------------
  \begin{center}
  \includegraphics[width=0.95\linewidth]{figs/communication_chain}
  \captionof{figure}{Simplified communication chain}
  \end{center}
  %------------------------------------------------

Leverage powerful, energy efficient procs (x86, ARM):
	\begin{enumerate}[label=\alph*.]
		\item Validate and optimize new algorithms
		\item Implement real systems with real time performance
			\begin{itemize}
				\item Enable Cloud computing-based architecture for Radio Access Networks 
	(C-RAN)    
				\item Reduce dev. cost and time to market
			\end{itemize}
	\end{enumerate}

	$\rightarrow$ Need for efficient software implementations to limit the energy consumption with high throughput.
}

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------
\headerbox{Turbo-coding / decoding}{name=methods,column=0,below=introduction}{
  A turbo code is a parallel concatenation of two component convolutional codes.\\
  The turbo-decoding process is an iterative method in which two soft-input soft-output (SISO) decoders exchange extrinsic information via an interleaver.

  %------------------------------------------------
  \begin{center}
  \includegraphics[width=0.95\linewidth]{figs/TurboDec}
  \captionof{figure}{Turbo-decoding process}
  \end{center}
  %------------------------------------------------
}

%----------------------------------------------------------------------------------------
%	CONTRIBUTIONS
%----------------------------------------------------------------------------------------
\headerbox{Contribution of this Work}{name=contrib,span=2,column=1,row=0}{ % To reduce this block to 1 column width, remove 'span=2'

	\begin{multicols}{2}

			\textbf{Two categories of parallelism in Turbo-decoding:}
			\begin{itemize}
				\item \textbf{Intra frame parallelism} : a single codeword is processed at the time. Computation within the turbo-decoding process are parallelized ( trellis transitions, BCJR computations, sub-blocks, ...)
				\item \textbf{Inter frame parallelism} : several frames are processed at the same time. This increases latency but allows more regular memory accesses.
			\end{itemize}

			\includegraphics[width=0.95\linewidth]{figs/reordering}
			\captionof{figure}{Inter frame parallelism mapped on SIMD units}
			
			\textbf{State of the art implementations:}
			
			\begin{itemize}
				\item Dedicated Hardware:
				\begin{itemize}
					\item \textbf{\textcolor{Gn2}{low energy, high throughput, low latency,} \textcolor{red}{low flexibility.}}
					\item Intra frame parallelism is usually exploited.
					\item Inter frame is inefficient since it requires the duplication of turbo-decoders.
				\end{itemize}
				\item GPU:
					\begin{itemize}
						\item \textbf{\textcolor{red}{high energy, moderate throughput, high latency,} \textcolor{Gn2}{high flexibility.}}
						\item Intra frame strategy can be used.
						\item Inter frame allows regular data access.
						\item ~10x slower and ~100x more power consuming than HW implementation.
					\end{itemize}
				\item CPU:
					\begin{itemize}
						\item \textbf{\textcolor{red}{high energy, moderate throughput, high latency,} \textcolor{Gn2}{high flexibility.}}
						\item Intra frame strategy can be used (similar to dedicated hardware)
						\item Inter frame strategy can be used (not reported to date)
					\end{itemize}
			\end{itemize}

	\end{multicols}


\textbf{Contribution :}

\medskip

In this work, we propose a \textbf{generic and flexible CPU implementation of a turbo decoder} that exclusively uses \textbf{inter-frame parallelism}. Experimental results show that our turbo decoder outperforms existing implementations in terms of throughput and energy efficiency.

}

%----------------------------------------------------------------------------------------
%	AFF3CT
%----------------------------------------------------------------------------------------
\headerbox{The software platform: A Fast Forward Error Correction Tool (AFF3CT)}{name=aff3ct,span=2,column=1,below=contrib}{ % To reduce this block to 1 column width, remove 'span=2'
  \begin{multicols}{2}
    \textbf{\textcolor{red}{AFF3CT}: a software dedicated to simulations of \\
      digital communications with channel coding}

    \begin{center}
      {\color{DB2}\url{http://aff3ct.github.io}}
    \end{center}    

    \begin{itemize}
      % \item {\color{DB2}\url{http://aff3ct.github.io}}
      \item Support different coding scheme: \textbf{Polar}, \textbf{Turbo}, 
        \textbf{Convolutional}, \textbf{Repeat and Accumulate} and \textbf{LDPC} 
        (coming soon)
      \medskip
      \item Very fast simulations, take advantage of today CPUs architecture 
        (\textbf{hundreds of Mb/s on Intel Core i5/7})
      \begin{itemize}
        \item Written in \texttt{C++11} (\textbf{SystemC/TLM support})
        \item Monte-Carlo \textbf{multi-threaded} simulations
        % \item Intensive use of the \textbf{SIMD} (SSE, AVX and NEON)
        % \item Minimal memory allocations
        \item Upto \textbf{1000 times faster than MATLAB} code
      \end{itemize}
      \smallskip
      \item \textbf{Portable}: run on Linux, Mac OS X and Windows
      \medskip
      \item \textbf{Open-source code} (under MIT license)
      \item For Turbo coding simulations, the following items are configurable: generator polynomial, interleaver, SISO heuristic (log-MAP, max-log-MAP,...), puncturing, trellis termination.
    \end{itemize}

    %------------------------------------------------
    \begin{center}
      \includegraphics[width=0.95\linewidth]{graphs/bfer_6144}
      \captionof{figure}{Simulated BER and FER of the $K=6144$, $R=1/3$ LTE Turbo code with AFF3CT}
    \end{center}
    %------------------------------------------------
  \end{multicols}
}

%----------------------------------------------------------------------------------------
%	EXPERIMENTS
%----------------------------------------------------------------------------------------
\headerbox{Experiments and Measurements}{name=results1,span=2,column=1,below=aff3ct}{ % To reduce this block to 1 column width, remove 'span=2'

{\small\resizebox{\linewidth}{!}{ 
\begin{tabular}{|r|r|r||r|r|r|r|r|r|r|r|r|r|r||r|r|r|r||r|r|r|}
  \hline
  \multicolumn{3}{|c||}{\multirow{2}{*}{}} & \multicolumn{11}{c||}{\multirow{2}{*}{\textbf{Hardware and decoder parameters}}} & \multicolumn{4}{c||}{\multirow{2}{*}{\textbf{Decoding performances}}} & \multicolumn{3}{c|}{\multirow{2}{*}{\textbf{Metrics}}} \\
  \multicolumn{3}{|c||}{}                  & \multicolumn{11}{c||}{}                                                          & \multicolumn{4}{c||}{}                                                & \multicolumn{3}{c|}{}                                  \\
  \hline
  \multicolumn{2}{|r|}{\textbf{Work}}                                                              & \textbf{Year}         & \textbf{Platform}                                      & \textbf{Arch.}                       & \textbf{TDP}                  & \textbf{Cores}               & \textbf{Freq.} & \textbf{Algorithm}       & \textbf{Pre.}       & \textbf{SIMD}       & \textbf{Inter}                & $\boldsymbol{K}$      & \textbf{Iters}     & \textbf{BER}                  & \textbf{FER}                  & \textbf{Lat.} & \textbf{Thr.}  & \textbf{NThr.} & \textbf{TNDC}  & $\boldsymbol{E_d}$ \\ %Real TNDC
  \cline{15-16}
  \multicolumn{2}{|r|}{}                                                                           &                       &                                                        &                                      & Watts                         & or SM                        & GHz            &                          & bit                 & length              & level                         &                       &                    & \multicolumn{2}{c|}{at 0.7 dB}                                & $\mu$s        & Mbps           & Mbps           &                & nJ                 \\ %(not normalized)
  \hline
  \hline
  \multirow{10}{*}{\rotatebox[origin=c]{90}{\textbf{GPU-based}}} & \cite{wu2010}                   & 2010                  & Tesla C1060                                            & \textit{Tesla}                       & 200                           & 15                           & 1.30           &  ML-MAP                  & 32                  & 16                  & 100                           & 6144                  & 5                  & 1e-04                         & \multicolumn{1}{c|}{$-$}      & 76800         & 8.0            &  2.1           & 0.135          & 5000               \\ %0.027
    \cline{2-21}
                                                                   & \cite{wu2011}                   & 2011                  & GTX 470                                                & \textit{Fermi}                       & 215                           & 14                           & 1.22           &  ML-MAP                  & 32                  & 32                  & 100                           & 6144                  & 5                  & 4e-05                         & \multicolumn{1}{c|}{$-$}      & 20827         & 29.5           &  8.6           & 0.270          & 1458               \\ %0.054
    \cline{2-21}
                                                                 & \cite{chinnici2012}             & 2012                  & Tesla C2050                                            & \textit{Fermi}                       & 247                           & 14                           & 1.15           &   L-MAP                  & 32                  & 32                  & 32                            & 11918                 & 5                  & \multicolumn{1}{c|}{$-$}      & \multicolumn{1}{c|}{$-$}      & 108965        & 3.5            &  1.1           & 0.035          & 14114              \\ %0.007
  \cline{2-21}
                                                                 & \cite{yoge2012}                 & 2012                  & 9800 GX2                                               & \textit{Tesla}                       & 197                           & 16                           & 1.50           &  ML-MAP                  & 32                  & 16                  & 1                             & 6144                  & 5                  & 1e-02                         & \multicolumn{1}{c|}{$-$}      & 3072          & 2.0            &  0.4           & 0.025          & 19700              \\ %0.005
  \cline{2-21}
                                                                 & \cite{liu2013}                  & 2013                  & GTX 550 Ti                                             & \textit{Fermi}                       & 116                           & 6                            & 1.80           & EML-MAP                  & 32                  & 32                  & 1                             & 6144                  & 6                  & 1e-02                         & \multicolumn{1}{c|}{$-$}      & 72$^*$        & 85.3           & 47.4           & 1.482          & 227                \\ %0.247
  \cline{2-21}
                                                                 & \cite{chen2013}                 & 2013                  & GTX 580                                                & \textit{Fermi}                       & 244                           & 16                           & 1.54           &  ML-MAP                  & 32                  & 32                  & 1                             & 6144                  & 6                  & 3e-04                         & \multicolumn{1}{c|}{$-$}      & 1660          & 3.7            &  0.9           & 0.030          & 10090              \\ %0.005
  \cline{2-21}
                                                                 & \cite{xianjun2013}              & 2013                  & GTX 480                                                & \textit{Fermi}                       & 250                           & 15                           & 1.40           & EML-MAP                  & 32                  & 32                  & 1                             & 6144                  & 6                  & \multicolumn{1}{c|}{$-$}      & \multicolumn{1}{c|}{$-$}      & 50$^*$        & 122.8          & 35.1           & 1.098          & 339                \\ %0.183
  \cline{2-21}
                                                                 & \cite{wu2013}                   & 2013                  & GTX 680                                                & \textit{Kepler}                      & 195                           & 8                            & 1.01           & EML-MAP                  & 32                  & 192                 & 16                            & 6144                  & 6                  & \multicolumn{1}{c|}{$-$}      & 1e-02                         & 2657          & 37.0           & 27.5           & 0.144          & 878                \\ %0.024
  \cline{2-21}
                                                                 & \cite{zhang2014}                & 2014                  & Tesla K20c                                             & \textit{Kepler}                      & 225                           & 13                           & 0.71           &  ML-MAP                  & 32                  & 192                 & 1                             & 6144                  & 5                  & 1e-04                         & \multicolumn{1}{c|}{$-$}      & 1097          & 5.6            &  3.0           & 0.015          & 8036               \\ %0.003
  \cline{2-21}
                                                                   & \cite{li2014}                   & 2014                  & GTX 580                                                & \textit{Fermi}                       & 244                           & 16                           & 1.54           & BR-SOVA                  & 8                   & 32                  & 4                             & 6144                  & 5                  & 2e-02                         & \multicolumn{1}{c|}{$-$}      & 192$^*$       & 127.8          & 25.9           & 0.810          & 382                \\ %0.162
    \hline
    \hline
      \multirow{9}{*}{\rotatebox[origin=c]{90}{\textbf{CPU-based}}}  & \cite{huang2011}                & 2011                  & i7-960                                                 & \textit{Nehalem}                     & 130                           & 1                            & 3.20           &  ML-MAP                  & 16                  & 8                   & 1                             & 1008                  & 8                  & 3e-03                         & 7e-02                         & 138           & 7.3            & 18.3           & 2.280          & 2226               \\ %0.285
      \cline{2-21}
                                                                     & \cite{zhang2012}                & 2012                  & X5670                                                  & \textit{Westmere}                    & 95                            & 6                            & 2.93           & EML-MAP                  & 8                   & 16                  & 6                             & 5824                  & 3                  & 6e-02                         & \multicolumn{1}{c|}{$-$}      & 157           & 222.6          & 38.0           & 2.373          & 142                \\ %0.791
      \cline{2-21}
                                                                       & \cite{wu2013}                   & 2013                  & i7-3770K                                               & \textit{Ivy Bridge}                  & 77                            & 4                            & 3.50           & EML-MAP                  & 16                  & 8                   & 4                             & 6144                  & 6                  & \multicolumn{1}{c|}{$-$}      & 1e-01                         & 323           & 76.2           & 32.7           & 4.080          & 168                \\ %0.680
        \cline{2-21}             
                                                                   &                                 & \multirow{6}{*}{2016} & E5-2650                                                & \textit{Ivy Bridge}                  & 95                            & 8                            & 2.50           & \multirow{6}{*}{EML-MAP} & \multirow{3}{*}{16} & \multirow{3}{*}{8}  & 64                            & \multirow{6}{*}{6144} & \multirow{6}{*}{6} & \multirow{3}{*}{6e-06}        & \multirow{3}{*}{6e-03}        & 3665          & 107.3          & 32.2           & 4.014          & 148                \\ %0.669 
    \cline{4-8} \cline{12-12} \cline{17-21}
                                                                   &                                 &                       & i7-4960HQ                                              & \textit{Haswell}                     & 47                            & 4                            & 3.20           &                          &                     &                     & 32                            &                       &                    &                               &                               & 2212          & 88.9           & 41.7           & 5.208          & 88                 \\ %0.868
    \cline{4-8} \cline{12-12} \cline{17-21}
                                                                   & this                            &                       & $2\times$E5-2680v3                                     & \textit{Haswell}                     & 240                           & 24                           & 2.50           &                          &                     &                     & 192                           &                       &                    &                               &                               & 2657          & 443.7          & 44.4           & 5.544          & 90                 \\ %0.924
    \cline{4-8} \cline{10-12} \cline{15-21}
                                                                   & work                            &                       & E5-2650                                                & \textit{Ivy Bridge}                  & 95                            & 8                            & 2.50           &                          & \multirow{3}{*}{8}  & \multirow{3}{*}{16} & 128                           &                       &                    & \multirow{3}{*}{8e-05}        & \multirow{3}{*}{5e-02}        & 3492          & 225.2          & 67.6           & 4.224          & 70                 \\ %0.704
    \cline{4-8} \cline{12-12} \cline{17-21}
                                                                   &                                 &                       & i7-4960HQ                                              & \textit{Haswell}                     & 47                            & 4                            & 3.20           &                          &                     &                     & 64                            &                       &                    &                               &                               & 2837          & 138.6          & 65.0           & 4.062          & 57                 \\ %0.677
    \cline{4-8} \cline{12-12} \cline{17-21}
                                                                   &                                 &                       & $2\times$E5-2680v3                                     & \textit{Haswell}                     & 240                           & 24                           & 2.50           &                          &                     &                     & 384                           &                       &                    &                               &                               & 3293          & 716.4          & 71.6           & 4.476          & 56                 \\ %0.746
    \hline
  \end{tabular}
  }}


}

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------
\headerbox{Loop merging in BCJR implementation}{name=conclusion,column=0,below=methods,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{
\medskip
Standard BCJR implementation:
\smallskip
{\footnotesize
\begin{algorithmic}[1]
\For{all frames}\Comment{Sequential loop}
  \For{$k=0;~k<K;~k=k+1$} \Comment{Parallel loop}
    \State $\boldsymbol{\gamma}^k\gets computeGamma(L_{sys}^k, L_{p}^k, L_{e}^k)$	
  \EndFor

  \State $\boldsymbol{\alpha}^0\gets initAlpha()$
  \For{$k=1;~k<K;~k=k+1$}  \Comment{Sequential loop}
    \State $\boldsymbol{\alpha}^k\gets computeAlpha(\boldsymbol{\alpha}^{k-1}, \boldsymbol{\gamma}^{k-1})$	
  \EndFor

  \State $\boldsymbol{\beta}^{K-1}\gets initBeta()$
  \For{$k=K-2;~k \geq 0;~k=k-1$}  \Comment{Sequential loop}
    \State $\boldsymbol{\beta}^k\gets computeBeta(\boldsymbol{\beta}^{k+1}, \boldsymbol{\gamma}^{k})$	
  \EndFor

  \For{$k=0;~k<K;~k=k+1$} \Comment{Parallel loop}
    \State $L_e^k\gets computeExtrinsic(\boldsymbol{\alpha}^k, \boldsymbol{\beta}^{k}, \boldsymbol{\gamma}^{k})$	
  \EndFor
\EndFor
\end{algorithmic}
}
\medskip
Loop merging BCJR implementation
\smallskip
{\footnotesize
\begin{algorithmic}[1]
\For{all frames} \Comment{Vectorized loop}
  \State $\boldsymbol{\alpha}^0\gets initAlpha()$
  \For{$k=1;~k<K;~k=k+1$} \Comment{Sequential loop}
    \State $\boldsymbol{\gamma}^{k-1}\gets computeGamma(L_{sys}^{k-1}, L_{p}^{k-1}, L_{e}^{k-1})$
    \State $\boldsymbol{\alpha}^k\gets computeAlpha(\boldsymbol{\alpha}^{k-1}, \boldsymbol{\gamma}^{k-1})$	
  \EndFor
  \State $\boldsymbol{\gamma}^{K-1}\gets computeGamma(L_{sys}^{K-1}, L_{p}^{K-1}, L_{e}^{K-1})$
  \State $\boldsymbol{\beta}^{K-1}\gets initBeta()$
  \State $L_e^{K-1}\gets computeExtrinsic(\boldsymbol{\alpha}^{K-1}, \boldsymbol{\beta}^{K-1}, \boldsymbol{\gamma}^{K-1})$
  \For{$k=K-2;~k \geq 0;~k=k-1$}  \Comment{Sequential loop}
    \State $\boldsymbol{\beta}^k\gets computeBeta(\boldsymbol{\beta}^{k+1}, \boldsymbol{\gamma}^{k})$
    \State $L_e^{k}\gets computeExtrinsic(\boldsymbol{\alpha}^{k}, \boldsymbol{\beta}^k, \boldsymbol{\gamma}^{k})$
  \EndFor
\EndFor
\end{algorithmic}
}

}

%----------------------------------------------------------------------------------------
% REFERENCES
%----------------------------------------------------------------------------------------
\headerbox{References}{name=references,below=conclusion,above=bottom,column=0}{
  \medskip
  %\smallskip
  \tiny
  \setlength{\bibsep}{0pt plus 0.3ex}
  %\smaller % Reduce the font size in this block
  \renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
  %\nocite{*} % Insert publications even if they are not cited in the poster

  \bibliographystyle{unsrt}
  \bibliography{article} % Use sample.bib as the bibliography file
}

%----------------------------------------------------------------------------------------
% CONTACT
%----------------------------------------------------------------------------------------
\headerbox{Conclusion}{name=contact,column=1,below=results1,above=bottom,headerColorOne=Gr2!50,headerColorTwo=Gr1!50,headerFontColor=Gr1}{
  \medskip
  In this work a generic and flexible CPU implementation of a turbo decoder that exclusively uses inter-frame parallelism. Experimental results show that our turbo decoder outperforms existing implementations in terms of throughput and energy efficiency.
}



%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------
\headerbox{Acknowledgements}{name=acknowledgements,column=2,below=results1,above=bottom, headerColorOne=Gr2!50,headerColorTwo=Gr1!50,headerFontColor=Gr1}{
  \smaller % Reduce the font size in this block
  \smallskip
  This study has been carried out with financial support from the French 
  State, managed by the French National Research Agency (ANR) in the frame 
  of the "Investments for the future" Programme IdEx Bordeaux - CPU 
  (\textbf{ANR-10-IDEX-03-02}).
} 

%----------------------------------------------------------------------------------------

\end{poster}

\end{document}
